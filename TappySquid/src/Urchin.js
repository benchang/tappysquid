var Urchin = cc.Sprite.extend ({
	ctor:function () {
		this._super(res.urchin_png);
		
		this.scale = .5; 
		
		this.x = 200;
		this.y = 100;
		
		var x1=this.x;
		var y1=this.y;
		
		var x2=this.x;
		var y2=400;
		
		
		var rise = new cc.EaseInOut ( new cc.MoveTo (3.0, x2,y2), 2.0 );
		
		var fall = new cc.EaseInOut ( new cc.MoveTo (3.0, x1,y1), 2.0 ) ;
		
		var floatAction = new cc.RepeatForever (new cc.Sequence (rise,fall));
		
		
		
		var breatheAction = new cc.RepeatForever (
		new cc.Sequence(new cc.ScaleTo (1.0 , 0.7),
							new cc.DelayTime (1.0),
							new cc.ScaleTo (3.0, 0.5)
							));
		
	
		
		this.runAction (breatheAction);
		this.runAction (floatAction);		
		
		
	}

});
