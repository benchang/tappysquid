var Squid = cc.Sprite.extend ({
	ctor: function () {
		this._super(res.squid_png);
	
		cc.spriteFrameCache.addSpriteFrame (new cc.SpriteFrame 
			(res.squid_png, cc.rect(0,0,100,137)),"squid1");
			
		cc.spriteFrameCache.addSpriteFrame (new cc.SpriteFrame 
			(res.squid2_png, cc.rect(0,0,123,140)),"squid2");
			
		cc.spriteFrameCache.addSpriteFrame (new cc.SpriteFrame 
			(res.squid3_png, cc.rect(0,0,95,171)),"squid3");
	
	
		
	
		cc.eventManager.addListener (
			cc.EventListener.create ({
				event: cc.EventListener.TOUCH_ONE_BY_ONE,
				swallowTouches: true,
				onTouchBegan: this.onTouchBegan,
				onTouchEnded: this.onTouchEnded
				}),this);
			
		
	
		return true;
	},
	
	onTouchBegan:function (touch, event) {
		cc.log ("touch began");
		
		var t=event.getCurrentTarget();

		var squidLoc = t.getPosition ();
		var touchLoc = touch.getLocationInView();
		
		// get a vector from current to new location
		//	delta = touchLoc - squidLoc; you can't do this :( 
		var delta = cc.pSub(touchLoc, squidLoc);
		
		var theta = cc.pToAngle(delta);
		var degrees = 90-cc.radiansToDegrees(theta);	
		
		t.setRotation(degrees);
		
		t.setSpriteFrame("squid2");
		
		//indicate that I've handled the event.  
		// stops propagation out to other handlers 
		return true;
	},
	onTouchEnded:function (touch, event) {
		cc.log ("touch ended");
		// you must get the current target to access the object that's 
		// handling the event (i.e. the squid).  'this' doesn't work here.
		
		var t=event.getCurrentTarget();

		var squidLoc = t.getPosition ();
		var touchLoc = touch.getLocationInView();		
			
		// cc.moveTo(1,touchLoc) is the same as 
		// new cc.MoveTo(1,touchLoc)
		
		t.stopAllActions();	
		t.setSpriteFrame("squid3");
		
		// add a callback function to reset to neutral image after the animation finishes
		var callback = new cc.CallFunc(t.doIdle, t);
		
		var seq = new cc.Sequence (
			new cc.EaseOut(new cc.MoveTo(1,touchLoc),3.0),
			callback);
		t.runAction (seq);
				
				
		return true;	
		
	},
	doIdle:function () {
		this.setSpriteFrame("squid1");
	},
	// you can also use the update method, which is called every frame
	// dt is the deltaTime, amount of time since last update call 
	// make sure to call this.scheduleUpdate() in the constructor 
	// to activate this if you want it
	update:function(dt) {
	
	}
});